# Job Images

A collection of Docker files and build pipelines for producing
Docker images that are commonly useful in other pipelines. Saves 
people from re-building the same environments in which to run their
pipeline.

Each folder should contain a Dockerfile and .gitlab-ci.yml fo
building the Docker image and pushing it to the GitLab registry.

## Using the images

Docker images built here can be used in your own pipelines like so:
```
image: genomicsengland/opensource/gitlab-tools/aws-tools:latest
```


### aws-tools 

A docker image with `aws-cli`, `jq`, `bash` installed. Should
be useful for running any Terraform or interacting with AWS 
resources.

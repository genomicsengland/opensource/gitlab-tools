# GitLab Tools

**A work in-progress!**

A collection of GitLab CI templates, scripts and docker images that can be used in pipelines
or automation of DevOps tasks.

Created to bring together common patterns and best practice examples to share across all squads.

Please contribute your ideas, code, and any issues you find so this can be a great resource
for GEL engineers. These can be raised directly on the repo from the Issues menu.

If you don't get active responses from issues raised here, then reach out
in the #devs slack channel at GEL.

## Contents

* `ci`: GitLab CI yaml template files to include in your pipelines
* `job-images`: Docker files and pipelines for building images with 
pre-installed tools useful in pipelines.

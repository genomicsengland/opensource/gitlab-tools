# GitLab CI Templates

In this folder are a collection of GitLab CI pipeline templates that 
can be included directly into your `.gitlab-ci.yml` or used as examples
for customisation and extension.

## Use

Include these template files in your `.gitlab.yml` by adding the following:

```
include:
  - project: 'genomicsengland/opensource/gitlab-tools'
    file: '/ci/<name>.yml'
```

Docs: https://docs.gitlab.com/ee/ci/yaml/includes.html
